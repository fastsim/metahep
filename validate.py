import numpy as np
from utils.observables import LongitudinalProfile, LateralProfile, Energy
from utils.plotters import ProfilePlotter, EnergyPlotter
from core.constants import N_CELLS_PHI, N_CELLS_R, N_CELLS_Z


def validate(e_layer_g4, e_layer_vae, particle_energy, particle_angle, geometry):
    # Reshape the events into 3D
    e_layer_vae = e_layer_vae.reshape(
        (len(e_layer_vae), N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
    )

    # Create observables from raw data.
    full_sim_long = LongitudinalProfile(_input=e_layer_g4)
    full_sim_lat = LateralProfile(_input=e_layer_g4)
    full_sim_energy = Energy(_input=e_layer_g4)
    ml_sim_long = LongitudinalProfile(_input=e_layer_vae)
    ml_sim_lat = LateralProfile(_input=e_layer_vae)
    ml_sim_energy = Energy(_input=e_layer_vae)

    # Plot observables
    longitudinal_profile_plotter = ProfilePlotter(
        particle_energy,
        particle_angle,
        geometry,
        full_sim_long,
        ml_sim_long,
        _plot_gaussian=False,
    )
    lateral_profile_plotter = ProfilePlotter(
        particle_energy,
        particle_angle,
        geometry,
        full_sim_lat,
        ml_sim_lat,
        _plot_gaussian=False,
    )
    energy_plotter = EnergyPlotter(
        particle_energy, particle_angle, geometry, full_sim_energy, ml_sim_energy
    )

    longitudinal_profile_plotter.plot_and_save()
    lateral_profile_plotter.plot_and_save()
    energy_plotter.plot_and_save()
