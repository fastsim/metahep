import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import backend as K

from core.constants import BATCH_SIZE_PER_REPLICA, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z


class _Sampling(layers.Layer):
    """Custom layer to perform the reparameterization trick.

    The reparameterization trick is used in variational autoencoders (VAEs) to sample random latent vectors z from
    the latent Gaussian distribution. This class takes three input tensors: the mean z_mean, the log variance z_log_var,
    and a noise tensor epsilon that is sampled from a standard normal distribution. The output tensor is the sampled
    vector z, given by sampled_z = z_mean + z_sigma * epsilon, where z_sigma = exp(0.5 * z_log_var).

    Args:
        inputs: A tuple of three tensors (z_mean, z_log_var, epsilon).
        **kwargs: Additional keyword arguments to pass to the parent class's `__call__` method.

    Returns:
        The tensor representing the sampled latent vector z.

    """

    def __call__(self, inputs, **kwargs):
        z_mean, z_log_var, epsilon = inputs
        z_sigma = K.exp(0.5 * z_log_var)
        return z_mean + z_sigma * epsilon


class _KLDivergenceLayer(layers.Layer):
    """Custom layer to compute the Kullback-Leibler divergence (KL divergence) between the distribution of the
    latent vector z and a prior distribution (a standard normal distribution).

    This layer takes two input tensors: the mean mu and the log variance log_var of the distribution of z.
    The output of the layer is the same as the input, but the KL divergence loss is also added as a regularization
    term to the model, which encourages the distribution of z to be close to the prior distribution.

    Args:
        inputs: A tuple of two tensors (mu, log_var).
        **kwargs: Additional keyword arguments to pass to the parent class's `call` method.

    Returns:
        The input tensors (mu, log_var) without modification.

    """

    def __call__(self, inputs, **kwargs):
        mu, log_var = inputs
        kl_loss = -0.5 * (1 + log_var - K.square(mu) - K.exp(log_var))
        kl_loss = K.mean(K.sum(kl_loss, axis=-1))
        self.add_loss(kl_loss)
        return inputs


def _PhysicsLosses(y_true, y_pred):
    """Custom loss function that measures the mean squared error (MSE) between the predicted and true energy
    deposition profiles of a particle in a detector.

    This loss function takes two input tensors: y_true and y_pred, both of shape
    (batch_size, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z), where N_CELLS_R, N_CELLS_PHI, and N_CELLS_Z are the number of
    cells in the r, phi, and z directions, respectively. The function first reshapes the input tensors to remove the
    batch dimension. It then computes the MSE between the true and predicted energy deposition profiles in the r-z and
    phi-z planes separately, and returns the sum of these two MSE terms.

    Args:
        y_true: The true energy deposition profile of the particle in the detector, represented as a tensor of shape
            (batch_size, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z).
        y_pred: The predicted energy deposition profile of the particle in the detector, represented as a tensor of
            shape (batch_size, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z).

    Returns:
        The total MSE loss between the true and predicted energy deposition profiles in the r-z and phi-z planes.

    """

    y_true = tf.reshape(y_true, (-1, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))
    y_pred = tf.reshape(y_pred, (-1, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))
    # longitudinal profile
    loss = MeanSquaredError(reduction=Reduction.SUM)(
        tf.reduce_sum(y_true, axis=(0, 1, 2)), tf.reduce_sum(y_pred, axis=(0, 1, 2))
    ) / (BATCH_SIZE_PER_REPLICA * N_CELLS_R * N_CELLS_PHI)
    # lateral profile
    loss += MeanSquaredError(reduction=Reduction.SUM)(
        tf.reduce_sum(y_true, axis=(0, 2, 3)), tf.reduce_sum(y_pred, axis=(0, 2, 3))
    ) / (BATCH_SIZE_PER_REPLICA * N_CELLS_Z * N_CELLS_PHI)
    return loss
