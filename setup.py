import os
import urllib.request

from core.constants import (
    INIT_DIR,
    GLOBAL_CHECKPOINT_DIR,
    CONV_DIR,
    VALID_DIR,
    GEN_DIR,
    PRETRAINED_DIR,
    FILE_URL,
    PRETRAINED_FILE_NAME,
)


for folder in [
    INIT_DIR,  # Directory to load the full simulation dataset
    GLOBAL_CHECKPOINT_DIR,  # Directory to save VAE checkpoints
    CONV_DIR,  # Directory to save model after conversion to a format that can be used in C++
    VALID_DIR,  # Directory to save validation plots
    GEN_DIR,  # Directory to save VAE generated showers
    PRETRAINED_DIR,  # Directory to save/load the pretrained model.
]:
    os.system(f"mkdir {folder}")

# Download the pretrained model & save it to the current directory
"""
try:
    urllib.request.urlretrieve(FILE_URL, f"{PRETRAINED_DIR}/{PRETRAINED_FILE_NAME}")
    print("File downloaded successfully.")
except Exception as e:
    print("An error occurred while downloading the file:", str(e))
"""
