import h5py
import numpy as np
from os import listdir
from os.path import isfile, join

from core.constants import (
    INIT_DIR,
    ORIGINAL_DIM,
    MAX_ENERGY,
    MAX_ANGLE,
    MIN_ANGLE,
    MIN_ENERGY,
    MAX_NB_EVENTS,
    GEO_NAME,
)


def preprocess():
    """
    Create four arrays: one array for voxel energies and three arrays for condition energy, angle and geometry.
    Args:
        None
    Returns:
        List of arrays.

    """
    energies_train = []
    cond_e_train = []
    cond_angle_train = []
    cond_geo_train = []
    for geo in [GEO_NAME]:
        dir_geo = INIT_DIR + "/" + geo + "/"
        # loop over the angles in a step of 10
        for angle_particle in range(MIN_ANGLE, MAX_ANGLE + 10, 10):
            # name of the h5 file
            f_name = f"{dir_geo}/{geo}_angle_{angle_particle}.h5"
            # read the HDF5 file
            h5 = h5py.File(f_name, "r")
            # loop over energies from min_energy to max_energy
            energy_particle = MIN_ENERGY
            while energy_particle <= MAX_ENERGY:
                # scale the energy of each voxel to the energy of the primary particle (in MeV units)
                events = np.array(h5[f"{energy_particle}"])[:MAX_NB_EVENTS] / (
                    energy_particle * 1000
                )
                # reshape the array to ORIGINAL_DIM = N_CELLS_Z * N_CELLS_R * N_CELLS_PHI
                energies_train.append(events.reshape(len(events), ORIGINAL_DIM))
                # build the energy and angle condition vectors
                cond_e_train.append(
                    [energy_particle / 4096] * len(events)
                )  # 4096: max energy in GeV
                cond_angle_train.append(
                    [angle_particle / 360] * len(events)
                )  # 360: max angle in degrees
                # build the geometry condition vector (1 hot encoding vector)
                if geo == "SiW":
                    cond_geo_train.append([[0, 0, 1]] * len(events))
                else:
                    cond_geo_train.append([[1, 0, 0]] * len(events))
                energy_particle *= 2
            h5.close()

    # return numpy arrays
    energies_train = np.concatenate(energies_train)
    cond_e_train = np.concatenate(cond_e_train)
    cond_angle_train = np.concatenate(cond_angle_train)
    cond_geo_train = np.concatenate(cond_geo_train)
    return energies_train, cond_e_train, cond_angle_train, cond_geo_train


def get_condition_arrays(geo, energy_particle, angle_particle, nb_events):
    """
    Returns condition values from a single geometry, a single energy and angle of primary particles.

    Args:
        - geo : name of the calorimeter geometry (eg: SiW)
        - energy_particle : energy of the primary particle in GeV units
        - angle_particle : angle of the primary particle in degrees
        - nb_events : number of events

    Returns:
        List of arrays representing condition values.

    """
    cond_e = [energy_particle / MAX_ENERGY] * nb_events
    cond_angle = [angle_particle / MAX_ANGLE] * nb_events
    if geo == "SiW":
        cond_geo = [[0, 0, 1]] * nb_events
    else:
        cond_geo = [[1, 0, 0]] * nb_events
    cond_e = np.array(cond_e)
    cond_angle = np.array(cond_angle)
    cond_geo = np.array(cond_geo)
    return cond_e, cond_angle, cond_geo


def load_showers(geo, angle_particle, energy_particle):
    """
    Loads events from a single geometry, a single energy and angle of primary particles

    Args:
        - init_dir: the name of the directory which contains the HDF5 files
        - geo : name of the calorimeter geometry (eg: SiW, SciPb)
        - energy_particle : energy of the primary particle in GeV units
        - angle_particle : angle of the primary particle in degrees

    Returns:
        Array of energies.

    """
    dir_geo = INIT_DIR + "/" + geo + "/"
    # name of the h5 file
    f_name = f"{dir_geo}/{geo}_angle_{angle_particle}.h5"
    # read the HDF5 file
    h5 = h5py.File(f_name, "r")
    energies = np.array(h5[f"{energy_particle}"])[:MAX_NB_EVENTS]
    h5.close()
    return energies
