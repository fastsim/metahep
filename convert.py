import argparse
import sys
import tf2onnx

from core.constants import GLOBAL_CHECKPOINT_DIR, CONV_DIR
from core.models.vae import VAE
from core.models import ModelHandler


def parse_args(argv):
    """
    Parse command line arguments.

    Args:
        argv (list): A list of command line arguments.
        adaptaion_step: adaptation step of the saved checkpoint model
        study_name: study name
        onnx_file_name: name of the ONNX converted model to save

    Returns:
        argparse.Namespace: An object containing parsed arguments.
    """
    p = argparse.ArgumentParser()
    p.add_argument("--adaptaion_step", type=int, default="")
    p.add_argument("--study_name", type=str, default="default_study_name")
    p.add_argument("--onnx_file_name", type=str, default="Generator.onnx")
    args = p.parse_args()
    return args


def main(argv):
    """
    Convert a Keras model to ONNX format.

    This function loads a saved Keras model, and converts it into an ONNX model
    format. The resulting ONNX model is saved in a specified directory.

    Args:
        argv (list): A list of command line arguments.

    Returns:
        None
    """

    # Parse commandline arguments
    args = parse_args(argv)
    adaptaion_step = args.adaptaion_step
    study_name = args.study_name
    onnx_file_name = args.onnx_file_name

    # Instantiate and load a saved model
    vae = VAE(ModelHandler)
    vae._build_and_compile_new_model()

    # Load the saved weights
    vae.model.load_weights(
        f"{GLOBAL_CHECKPOINT_DIR}/{study_name}/model_weights_adaptation_step_{adaptaion_step}.h5"
    )

    # Convert the model to ONNX format
    # Create the Keras model and convert it into an ONNX model
    keras_model = vae.model.decoder
    output_path = f"{CONV_DIR}/{onnx_file_name}"
    tf2onnx.convert.from_keras(keras_model, output_path=output_path)

    # print a success message after conversion
    print(f"{onnx_file_name} is now saved in{CONV_DIR}.")


if __name__ == "__main__":
    exit(main(sys.argv[1:]))
